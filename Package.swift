// swift-tools-version:4.0

import PackageDescription

let package = Package(
    name: "acdump",
    products: [
        .executable(name: "acdump", targets: ["acdump"]),
    ],
    dependencies: [
        .package(url: "https://github.com/jkolb/Asheron", from: "0.0.0"),
        .package(url: "https://github.com/jkolb/Lilliput", from: "8.0.0"),
    ],
    targets: [
        .target(name: "acdump", dependencies: ["Asheron", "Lilliput"]),
    ]
)
