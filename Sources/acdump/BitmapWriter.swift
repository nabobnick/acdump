//
//  BitmapWriter.swift
//  Nostalgia
//
//  Created by Justin Kolb on 11/5/16.
//
//

import Asheron
import Lilliput

public class BitmapWriter {
    public func write(bitmap: BitmapV4) -> ByteBuffer {
        let buffer = OrderedByteBuffer<LittleEndian>(count: numericCast(bitmap.fileHeader.size))
        writeFileHeader(bitmap.fileHeader, to: buffer)
        writeInfoHeader(bitmap.infoHeader, to: buffer)
        writeBitmapData(bitmap.bitmapData, to: buffer)
        return buffer.buffer
    }
    
    private func writeFileHeader(_ fileHeader: BitmapFileHeader, to buffer: OrderedByteBuffer<LittleEndian>) {
        "BM".withCString(encodedAs: UTF8.self) { (chars) -> Void in
            buffer.putUInt8(chars[0])
            buffer.putUInt8(chars[1])
        }
        buffer.putUInt32(fileHeader.size)
        buffer.putUInt16(0) // Reserved1
        buffer.putUInt16(0) // Reserved2
        buffer.putUInt32(fileHeader.offBits)
    }
    
    private func writeInfoHeader(_ infoHeader: BitmapV4Header, to buffer: OrderedByteBuffer<LittleEndian>) {
        buffer.putUInt32(UInt32(infoHeader.size))
        buffer.putInt32(infoHeader.width)
        buffer.putInt32(infoHeader.height)
        buffer.putUInt16(infoHeader.planes)
        buffer.putUInt16(infoHeader.bitCount.rawValue)
        buffer.putUInt32(infoHeader.compression.rawValue)
        buffer.putUInt32(infoHeader.sizeImage)
        buffer.putInt32(infoHeader.xPelsPerMeter)
        buffer.putInt32(infoHeader.yPelsPerMeter)
        buffer.putUInt32(infoHeader.clrUsed)
        buffer.putUInt32(infoHeader.clrImportant)
        buffer.putUInt32(infoHeader.redMask)
        buffer.putUInt32(infoHeader.greenMask)
        buffer.putUInt32(infoHeader.blueMask)
        buffer.putUInt32(infoHeader.alphaMask)
        buffer.putUInt32(infoHeader.csType)
        writeCIEXYZTRIPLE(infoHeader.endpoints, to: buffer)
        buffer.putUInt32(infoHeader.gammaRed)
        buffer.putUInt32(infoHeader.gammaGreen)
        buffer.putUInt32(infoHeader.gammaBlue)
    }
    
    private func writeBitmapData(_ bitmapData: ByteBuffer, to buffer: OrderedByteBuffer<LittleEndian>) {
        buffer.copy(from: bitmapData)
    }
    
    private func writeCIEXYZTRIPLE(_ triple: CIEXYZTRIPLE, to buffer: OrderedByteBuffer<LittleEndian>) {
        writeCIEXYZ(triple.red, to: buffer)
        writeCIEXYZ(triple.green, to: buffer)
        writeCIEXYZ(triple.blue, to: buffer)
    }
    
    private func writeCIEXYZ(_ ciexyz: CIEXYZ, to buffer: OrderedByteBuffer<LittleEndian>) {
        buffer.putUInt32(ciexyz.x)
        buffer.putUInt32(ciexyz.y)
        buffer.putUInt32(ciexyz.z)
    }
}
