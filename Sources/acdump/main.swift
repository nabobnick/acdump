import Asheron
import Lilliput

print(CommandLine.arguments)

var flags = Set<String>()
var options = [String:String]()

for index in 1..<CommandLine.arguments.count {
    let value = CommandLine.arguments[index]
    let parts = value.split(separator: "=").map(String.init)
    
    if parts.count == 1 {
        flags.insert(parts[0])
    }
    else {
        options[parts[0]] = parts[1]
    }
}

print(flags)
print(options)

let portalPath = options["portal"] ?? "Data/client_portal.dat"
let highresPath = options["highres"] ?? "Data/client_highres.dat"
let cellPath = options["cell"] ?? "Data/client_cell_1.dat"

let portalIndexFile: IndexFile
let highresIndexFile: IndexFile
let cellIndexFile: IndexFile

do {
    portalIndexFile = try IndexFile.openForReading(at: portalPath)
}
catch {
    fatalError("Unable to open \(portalPath)")
}
do {
    highresIndexFile = try IndexFile.openForReading(at: highresPath)
}
catch {
    fatalError("Unable to open \(highresPath)")
}
do {
    cellIndexFile = try IndexFile.openForReading(at: cellPath)
}
catch {
    fatalError("Unable to open \(cellPath)")
}

let portalFile = PortalFile(indexFile: portalIndexFile)
let highresFile = HighresFile(indexFile: highresIndexFile)
let cellFile = CellFile(indexFile: cellIndexFile)

if let convert = options["convert"] {
    if convert == "texture" {
        if let destination = options["destination"] {
            let textureHandles = try! portalIndexFile.handles(matching: { ($0 & 0xFFFF0000) == PortalKind.textureData.rawValue })
            let converter = TextureToBitmapConverter()
            let bitmapWriter = BitmapWriter()
            
            // Crashed on 0x06005ece unexpected texture format
            for handle in textureHandles {
                let texture = try! portalFile.fetchTextureData(handle: PortalHandle(rawValue: handle)!)
                if let bitmap = try! converter.makeV4Bitmap(texture: texture, portalFile: portalFile) {
                    let data = bitmapWriter.write(bitmap: bitmap)
                    let name = destination + "/" + hex(handle) + ".bmp"
                    let file = try BinaryFile.open(forWritingAtPath: name, create: true)
                    let writeCount = try file.write(from: data)
                    precondition(writeCount == data.count)
                    print(name)
                }
                else {
                    let name = destination + "/" + hex(handle) + ".jpg"
                    let file = try BinaryFile.open(forWritingAtPath: name, create: true)
                    let writeCount = try file.write(from: texture.data)
                    precondition(writeCount == texture.data.count)
                    print(name)
                }
            }
        }
        else {
            print("Missing destination")
        }
    }
    else if convert == "alpha" {
        if let destination = options["destination"] {
            let textureHandles = try! portalIndexFile.handles(matching: { ($0 & 0xFFFF0000) == PortalKind.textureData.rawValue })
            let converter = TextureToBitmapConverter()
            let bitmapWriter = BitmapWriter()
            
            // Crashed on 0x06005ece unexpected texture format
            for handle in textureHandles {
                let texture = try! portalFile.fetchTextureData(handle: PortalHandle(rawValue: handle)!)

                switch texture.format {
                    case .a8: break
                    default: continue
                }

                if let bitmap = try! converter.makeV4Bitmap(texture: texture, portalFile: portalFile) {
                    let data = bitmapWriter.write(bitmap: bitmap)
                    let name = destination + "/" + hex(handle) + ".bmp"
                    let file = try BinaryFile.open(forWritingAtPath: name, create: true)
                    let writeCount = try file.write(from: data)
                    precondition(writeCount == data.count)
                    print(name)
                }
                else {
                    let name = destination + "/" + hex(handle) + ".jpg"
                    let file = try BinaryFile.open(forWritingAtPath: name, create: true)
                    let writeCount = try file.write(from: texture.data)
                    precondition(writeCount == texture.data.count)
                    print(name)
                }
            }
        }
        else {
            print("Missing destination")
        }
    }
    else if convert == "intensity" {
        if let destination = options["destination"] {
            let textureHandles = try! portalIndexFile.handles(matching: { ($0 & 0xFFFF0000) == PortalKind.textureData.rawValue })
            let converter = TextureToBitmapConverter()
            let bitmapWriter = BitmapWriter()
            
            // Crashed on 0x06005ece unexpected texture format
            for handle in textureHandles {
                let texture = try! portalFile.fetchTextureData(handle: PortalHandle(rawValue: handle)!)

                switch texture.format {
                    case .i8: break
                    default: continue
                }

                if let bitmap = try! converter.makeV4Bitmap(texture: texture, portalFile: portalFile) {
                    let data = bitmapWriter.write(bitmap: bitmap)
                    let name = destination + "/" + hex(handle) + ".bmp"
                    let file = try BinaryFile.open(forWritingAtPath: name, create: true)
                    let writeCount = try file.write(from: data)
                    precondition(writeCount == data.count)
                    print(name)
                }
                else {
                    let name = destination + "/" + hex(handle) + ".jpg"
                    let file = try BinaryFile.open(forWritingAtPath: name, create: true)
                    let writeCount = try file.write(from: texture.data)
                    precondition(writeCount == texture.data.count)
                    print(name)
                }
            }

            let textureHandles2 = try! highresIndexFile.handles(matching: { ($0 & 0xFFFF0000) == PortalKind.textureData.rawValue })
            
            // Crashed on 0x06005ece unexpected texture format
            for handle in textureHandles2 {
                let texture = try! highresFile.fetchTextureData(handle: PortalHandle(rawValue: handle)!)

                switch texture.format {
                    case .i8: break
                    default: continue
                }

                if let bitmap = try! converter.makeV4Bitmap(texture: texture, portalFile: portalFile) {
                    let data = bitmapWriter.write(bitmap: bitmap)
                    let name = destination + "/" + hex(handle) + ".bmp"
                    let file = try BinaryFile.open(forWritingAtPath: name, create: true)
                    let writeCount = try file.write(from: data)
                    precondition(writeCount == data.count)
                    print(name)
                }
                else {
                    let name = destination + "/" + hex(handle) + ".jpg"
                    let file = try BinaryFile.open(forWritingAtPath: name, create: true)
                    let writeCount = try file.write(from: texture.data)
                    precondition(writeCount == texture.data.count)
                    print(name)
                }
            }
        }
        else {
            print("Missing destination")
        }
    }
    else if convert == "heightmap" {
        if let destination = options["destination"] {
            Convert().heightmap(cellFile: cellFile, destination: destination)
        }
        else {
            print("Missing destination")
        }
    }
    else if convert == "map" {
        if let destination = options["destination"] {
            Convert().map(portalFile: portalFile, cellFile: cellFile, destination: destination)
        }
        else {
            print("Missing destination")
        }
    }
    else {
        print("Can only convert texture, heightmap, map")
    }
}
else if let dump = options["dump"] {
    if let destination = options["destination"] {
        let handle = UInt32(dump, radix: 16)!
        let data = try! portalIndexFile.readData(handle: handle)
        let file = try BinaryFile.open(forWritingAtPath: destination + "/" + hex(handle) + ".dat", create: true)
        let writeCount = try file.write(from: data)
        precondition(writeCount == data.count)
    }
    else {
        print("Missing destination")
    }
}
else if let debug = options["debug"] {
    if debug == "WorldRegion" {
        let rawHandles = try! portalIndexFile.handles(matching: { ($0 & 0xFFFF0000) == PortalKind.worldRegion.rawValue })

        for rawHandle in rawHandles {
            let worldRegion = try! portalFile.fetchWorldRegion(handle: PortalHandle(rawValue: rawHandle)!)

            print("\(worldRegion.number): \(worldRegion.name) v\(worldRegion.version)")
            for height in worldRegion.landHeights {
                print("\(height)")
            }
        }
    }
    else if debug == "map" {
        if let destination = options["destination"] {
            Convert().map(portalFile: portalFile, cellFile: cellFile, destination: destination, namePrefix: "debug", spacing: 1)
        }
        else {
            print("Missing destination")
        }
    }
    else if debug == "biomes" {
        guard let destination = options["destination"] else {
            fatalError("Missing destination")
        }

        let converter = TextureToBitmapConverter()
        let bitmapWriter = BitmapWriter()
        let textureLoader = TextureLoader(portalFile: portalFile, highresFile: highresFile)
        let rawHandles = try! portalIndexFile.handles(matching: { ($0 & 0xFFFF0000) == PortalKind.worldRegion.rawValue })

        for rawHandle in rawHandles {
            let worldRegion = try! portalFile.fetchWorldRegion(handle: PortalHandle(rawValue: rawHandle)!)

            print("\(worldRegion.number): \(worldRegion.name) v\(worldRegion.version)")
            for biomeTexture in worldRegion.biomeTextures {
                let texture1 = try! textureLoader.fetchTextureData(handle: biomeTexture.textureListHandle1)

                if let bitmap = try! converter.makeV4Bitmap(texture: texture1, portalFile: portalFile) {
                    let data = bitmapWriter.write(bitmap: bitmap)
                    let name = destination + "/" + hex(texture1.handle.rawValue) + "_1.bmp"
                    let file = try BinaryFile.open(forWritingAtPath: name, create: true)
                    let writeCount = try file.write(from: data)
                    precondition(writeCount == data.count)
                    print(name)
                }
                else {
                    let name = destination + "/" + hex(texture1.handle.rawValue) + "_1.jpg"
                    let file = try BinaryFile.open(forWritingAtPath: name, create: true)
                    let writeCount = try file.write(from: texture1.data)
                    precondition(writeCount == texture1.data.count)
                    print(name)
                }

                let texture2 = try! textureLoader.fetchTextureData(handle: biomeTexture.textureListHandle2)

                if let bitmap = try! converter.makeV4Bitmap(texture: texture2, portalFile: portalFile) {
                    let data = bitmapWriter.write(bitmap: bitmap)
                    let name = destination + "/" + hex(texture2.handle.rawValue) + "_2.bmp"
                    let file = try BinaryFile.open(forWritingAtPath: name, create: true)
                    let writeCount = try file.write(from: data)
                    precondition(writeCount == data.count)
                    print(name)
                }
                else {
                    let name = destination + "/" + hex(texture2.handle.rawValue) + "_2.jpg"
                    let file = try BinaryFile.open(forWritingAtPath: name, create: true)
                    let writeCount = try file.write(from: texture2.data)
                    precondition(writeCount == texture2.data.count)
                    print(name)
                }
            }
        }
    }
    else if debug == "blend" {
        guard let destination = options["destination"] else {
            fatalError("Missing destination")
        }

        let converter = TextureToBitmapConverter()
        let bitmapWriter = BitmapWriter()
        let textureLoader = TextureLoader(portalFile: portalFile, highresFile: highresFile)
         let rawHandles = try! portalIndexFile.handles(matching: { ($0 & 0xFFFF0000) == PortalKind.worldRegion.rawValue })

        for rawHandle in rawHandles {
            let worldRegion = try! portalFile.fetchWorldRegion(handle: PortalHandle(rawValue: rawHandle)!)
            for (index, blendTexture) in worldRegion.cornerBlendTextures.enumerated() {
                let texture = try! textureLoader.fetchTextureData(handle: blendTexture.textureListHandle)

                if let bitmap = try! converter.makeV4Bitmap(texture: texture, portalFile: portalFile) {
                    let data = bitmapWriter.write(bitmap: bitmap)
                    let name = destination + "/biome_\(blendTexture.type)_\(index)_\(texture.handle).bmp"
                    let file = try BinaryFile.open(forWritingAtPath: name, create: true)
                    let writeCount = try file.write(from: data)
                    precondition(writeCount == data.count)
                }
                else {
                    let name = destination + "/biome_\(blendTexture.type)_\(index)_\(texture.handle).jpg"
                    let file = try BinaryFile.open(forWritingAtPath: name, create: true)
                    let writeCount = try file.write(from: texture.data)
                    precondition(writeCount == texture.data.count)
                    print(name)
                }
            }

            for (index, blendTexture) in worldRegion.straightBlendTextures.enumerated() {
                let texture = try! textureLoader.fetchTextureData(handle: blendTexture.textureListHandle)

                if let bitmap = try! converter.makeV4Bitmap(texture: texture, portalFile: portalFile) {
                    let data = bitmapWriter.write(bitmap: bitmap)
                    let name = destination + "/biome_\(blendTexture.type)_\(index)_\(texture.handle).bmp"
                    let file = try BinaryFile.open(forWritingAtPath: name, create: true)
                    let writeCount = try file.write(from: data)
                    precondition(writeCount == data.count)
                }
                else {
                    let name = destination + "/biome_\(blendTexture.type)_\(index)_\(texture.handle).jpg"
                    let file = try BinaryFile.open(forWritingAtPath: name, create: true)
                    let writeCount = try file.write(from: texture.data)
                    precondition(writeCount == texture.data.count)
                    print(name)
                }
            }

            for (index, blendTexture) in worldRegion.roadBlendTextures.enumerated() {
                let texture = try! textureLoader.fetchTextureData(handle: blendTexture.textureListHandle)

                if let bitmap = try! converter.makeV4Bitmap(texture: texture, portalFile: portalFile) {
                    let data = bitmapWriter.write(bitmap: bitmap)
                    let name = destination + "/road_\(blendTexture.type)_\(index)_\(texture.handle).bmp"
                    let file = try BinaryFile.open(forWritingAtPath: name, create: true)
                    let writeCount = try file.write(from: data)
                    precondition(writeCount == data.count)
                }
                else {
                    let name = destination + "/road_\(blendTexture.type)_\(index)_\(texture.handle).jpg"
                    let file = try BinaryFile.open(forWritingAtPath: name, create: true)
                    let writeCount = try file.write(from: texture.data)
                    precondition(writeCount == texture.data.count)
                    print(name)
                }
            }
        }
    }
    else {
        print("Unexpected debug: \(debug)")
    }
}
else if let handles = options["handles"] {
    if handles == "portal" {
        let handleList = try! portalIndexFile.handles(matching: { $0 == $0 })
        var counts = [String:Int]()
        
        for handle in handleList {
            let type = hex(handle & 0xFFFF0000)
            
            if let count = counts[type] {
                counts[type] = count + 1
            }
            else {
                counts[type] = 1
            }
        }
        
        print(counts)
    }
    else if handles == "highres" {
        let handleList = try! highresIndexFile.handles(matching: { $0 == $0 })
        var counts = [String:Int]()
        
        for handle in handleList {
            let type = hex(handle & 0xFFFF0000)
            
            if let count = counts[type] {
                counts[type] = count + 1
            }
            else {
                counts[type] = 1
            }
        }
        
        print(counts)
    }
    else if handles == "cell" {
        let handleList = try! cellIndexFile.handles(matching: { $0 == $0 })
        var indicesByType = [String:[String]]()
        
        for handle in handleList {
            let type = hex(UInt16((handle & 0xFFFF0000) >> 16))
            let index = hex(UInt16(handle & 0x0000FFFF))

            if var indices = indicesByType[type] {
                indices.append(index)
                indicesByType[type] = indices
            }
            else {
                let indices = [index]
                indicesByType[type] = indices
            }
        }

        indicesByType.flatMap({$0}).sorted(by: {$0.0 < $1.0}).forEach {
            print("\($0): \($1)")
        }
    }
    else {
        print("Unexpected handles request: \(handles)")
    }
}
else {
    print("Unknown action")
}
