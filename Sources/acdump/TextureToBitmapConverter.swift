//
//  TextureToBitmapConverter.swift
//  Nostalgia
//
//  Created by Justin Kolb on 11/5/16.
//
//

import Asheron
import Lilliput

public final class TextureToBitmapConverter {
    public func makeV4Bitmap(texture: TextureData, portalFile: PortalFile) throws -> BitmapV4? {
        let bitmapData: ByteBuffer
        
        switch texture.format {
        case .rgb888:
            bitmapData = PixelARGB8888.convert(width: Int(texture.width), height: Int(texture.height), data: texture.data, reader: PixelRGB888Reader())
            
        case .argb8888:
            bitmapData = PixelARGB8888.convert(width: Int(texture.width), height: Int(texture.height), data: texture.data, reader: PixelARGB8888Reader())
            
        case .p16(let handle):
            let colorTable = try portalFile.fetchColorTable(handle: handle)
            bitmapData = PixelARGB8888.convert(width: Int(texture.width), height: Int(texture.height), data: texture.data, reader: PixelP16Reader(colorTable: colorTable))
            
        case .p8(let handle):
            let colorTable = try portalFile.fetchColorTable(handle: handle)
            bitmapData = PixelARGB8888.convert(width: Int(texture.width), height: Int(texture.height), data: texture.data, reader: PixelP8Reader(colorTable: colorTable))
            
        case .i8:
            bitmapData = PixelARGB8888.convert(width: Int(texture.width), height: Int(texture.height), data: texture.data, reader: PixelI8Reader())
            
        case .bgr888:
            bitmapData = PixelARGB8888.convert(width: Int(texture.width), height: Int(texture.height), data: texture.data, reader: PixelBGR888Reader())
            
        case .rgb565:
            bitmapData = PixelARGB8888.convert(width: Int(texture.width), height: Int(texture.height), data: texture.data, reader: PixelRGB565Reader())
            
        case .argb4444:
            bitmapData = PixelARGB8888.convert(width: Int(texture.width), height: Int(texture.height), data: texture.data, reader: PixelARGB4444Reader())
            
        case .dxt1:
            bitmapData = DXT.decompress(width: Int(texture.width), height: Int(texture.height), data: texture.data, reader: DXT1Reader())
            
        case .dxt3:
            bitmapData = DXT.decompress(width: Int(texture.width), height: Int(texture.height), data: texture.data, reader: DXT3Reader())
            
        case .dxt5:
            bitmapData = DXT.decompress(width: Int(texture.width), height: Int(texture.height), data: texture.data, reader: DXT5Reader())
            
        case .jfif:
            return nil
            
        case .a8:
            bitmapData = PixelARGB8888.convert(width: Int(texture.width), height: Int(texture.height), data: texture.data, reader: PixelA8Reader())
        }
        
        let infoHeader = makeV4Header(texture: texture, bitmapData: bitmapData)
        let fileHeader = makeFileHeader(infoHeader: infoHeader, bitmapData: bitmapData)
        
        return BitmapV4(fileHeader: fileHeader, infoHeader: infoHeader, bitmapData: bitmapData)
    }
    
    private func makeFileHeader(infoHeader: BitmapInfoHeader, bitmapData: ByteBuffer) -> BitmapFileHeader {
        var fileHeader = BitmapFileHeader()
        let colorSize: UInt32 = 0
        fileHeader.size = BitmapFileHeader.byteSize + infoHeader.size + colorSize + UInt32(bitmapData.count)
        fileHeader.offBits = BitmapFileHeader.byteSize + infoHeader.size + colorSize
        return fileHeader
    }
    
    private func makeV4Header(texture: TextureData, bitmapData: ByteBuffer) -> BitmapV4Header {
        var infoHeader = BitmapV4Header()
        infoHeader.width = Int32(texture.width)
        infoHeader.height = -Int32(texture.height)
        infoHeader.sizeImage = UInt32(bitmapData.count)
        return infoHeader
    }
}
