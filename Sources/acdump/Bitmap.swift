import Asheron
import Lilliput

public struct BitmapFileHeader {
    public static var byteSize: UInt32 = 14
    public var size: UInt32 = 0
    public var offBits: UInt32 = 0
}

public protocol BitmapInfoHeader {
    var size: UInt32 { get }
}

public struct BitmapV4Header : BitmapInfoHeader {
    public let size: UInt32 = 108 // The number of bytes required by the structure.
    
    public var width: Int32 = 0 // The width of the bitmap, in pixels.
    public var height: Int32 = 0 // The height of the bitmap, in pixels. If biHeight is positive, the bitmap is a bottom-up DIB and its origin is the lower-left corner. If biHeight is negative, the bitmap is a top-down DIB and its origin is the upper-left corner. If biHeight is negative, indicating a top-down DIB, biCompression must be either BI_RGB or BI_BITFIELDS. Top-down DIBs cannot be compressed.
    public var planes: UInt16 = 1 // The number of planes for the target device. This value must be set to 1.
    public var bitCount: BitmapBitCount = .thirtytwo
    public var compression: BitmapCompression = .bitfields
    public var sizeImage: UInt32 = 0 // The size, in bytes, of the image. This may be set to zero for BI_RGB bitmaps.
    public var xPelsPerMeter: Int32 = 0 // The horizontal resolution, in pixels-per-meter, of the target device for the bitmap. An application can use this value to select a bitmap from a resource group that best matches the characteristics of the current device.
    public var yPelsPerMeter: Int32 = 0 // The vertical resolution, in pixels-per-meter, of the target device for the bitmap.
    public var clrUsed: UInt32 = 0 // The number of color indexes in the color table that are actually used by the bitmap. If this value is zero, the bitmap uses the maximum number of colors corresponding to the value of the biBitCount member for the compression mode specified by biCompression. If biClrUsed is nonzero and the biBitCount member is less than 16, the biClrUsed member specifies the actual number of colors the graphics engine or device driver accesses. If biBitCount is 16 or greater, the biClrUsed member specifies the size of the color table used to optimize performance of the system color palettes. If biBitCount equals 16 or 32, the optimal color palette starts immediately following the three DWORD masks. When the bitmap array immediately follows the BITMAPINFO structure, it is a packed bitmap. Packed bitmaps are referenced by a single pointer. Packed bitmaps require that the biClrUsed member must be either zero or the actual size of the color table.
    public var clrImportant: UInt32 = 0 // The number of color indexes that are required for displaying the bitmap. If this value is zero, all colors are required.
    public var redMask: UInt32   = 0b0000_0000_1111_1111_0000_0000_0000_0000
    public var greenMask: UInt32 = 0b0000_0000_0000_0000_1111_1111_0000_0000
    public var blueMask: UInt32  = 0b0000_0000_0000_0000_0000_0000_1111_1111
    public var alphaMask: UInt32 = 0b1111_1111_0000_0000_0000_0000_0000_0000
    public var csType: UInt32 = 1 // Device-dependent RGB (everything following is not used)
    public var endpoints: CIEXYZTRIPLE = CIEXYZTRIPLE()
    public var gammaRed: UInt32 = 0
    public var gammaGreen: UInt32 = 0
    public var gammaBlue: UInt32 = 0
}

public enum BitmapBitCount : UInt16 {
    case none = 0 // The number of bits-per-pixel is specified or is implied by the JPEG or PNG format.
    case one =  1 // The bitmap is monochrome, and the bmiColors member of BITMAPINFO contains two entries. Each bit in the bitmap array represents a pixel. If the bit is clear, the pixel is displayed with the color of the first entry in the bmiColors table; if the bit is set, the pixel has the color of the second entry in the table.
    case four = 4 // The bitmap has a maximum of 16 colors, and the bmiColors member of BITMAPINFO contains up to 16 entries. Each pixel in the bitmap is represented by a 4-bit index into the color table. For example, if the first byte in the bitmap is 0x1F, the byte represents two pixels. The first pixel contains the color in the second table entry, and the second pixel contains the color in the sixteenth table entry.
    case sixteen = 16 // The bitmap has a maximum of 2^16 colors. If the biCompression member of the BITMAPINFOHEADER is BI_RGB, the bmiColors member of BITMAPINFO is NULL. Each WORD in the bitmap array represents a single pixel. The relative intensities of red, green, and blue are represented with five bits for each color component. The value for blue is in the least significant five bits, followed by five bits each for green and red. The most significant bit is not used. The bmiColors color table is used for optimizing colors used on palette-based devices, and must contain the number of entries specified by the biClrUsed member of the BITMAPINFOHEADER. If the biCompression member of the BITMAPINFOHEADER is BI_BITFIELDS, the bmiColors member contains three DWORD color masks that specify the red, green, and blue components, respectively, of each pixel. Each WORD in the bitmap array represents a single pixel. When the biCompression member is BI_BITFIELDS, bits set in each DWORD mask must be contiguous and should not overlap the bits of another mask. All the bits in the pixel do not have to be used.
    case twentyfour = 24 // The bitmap has a maximum of 2^24 colors, and the bmiColors member of BITMAPINFO is NULL. Each 3-byte triplet in the bitmap array represents the relative intensities of blue, green, and red, respectively, for a pixel. The bmiColors color table is used for optimizing colors used on palette-based devices, and must contain the number of entries specified by the biClrUsed member of the BITMAPINFOHEADER.
    case thirtytwo = 32 // The bitmap has a maximum of 2^32 colors. If the biCompression member of the BITMAPINFOHEADER is BI_RGB, the bmiColors member of BITMAPINFO is NULL. Each DWORD in the bitmap array represents the relative intensities of blue, green, and red for a pixel. The value for blue is in the least significant 8 bits, followed by 8 bits each for green and red. The high byte in each DWORD is not used. The bmiColors color table is used for optimizing colors used on palette-based devices, and must contain the number of entries specified by the biClrUsed member of the BITMAPINFOHEADER. If the biCompression member of the BITMAPINFOHEADER is BI_BITFIELDS, the bmiColors member contains three DWORD color masks that specify the red, green, and blue components, respectively, of each pixel. Each DWORD in the bitmap array represents a single pixel. When the biCompression member is BI_BITFIELDS, bits set in each DWORD mask must be contiguous and should not overlap the bits of another mask. All the bits in the pixel do not need to be used.
}

public enum BitmapCompression : UInt32 {
    case rgb = 0 // An uncompressed format.
    case rle8 = 1 // A run-length encoded (RLE) format for bitmaps with 8 bpp. The compression format is a 2-byte format consisting of a count byte followed by a byte containing a color index. For more information, see Bitmap Compression.
    case rle4 = 2 // An RLE format for bitmaps with 4 bpp. The compression format is a 2-byte format consisting of a count byte followed by two word-length color indexes. For more information, see Bitmap Compression.
    case bitfields = 3 // Specifies that the bitmap is not compressed and that the color table consists of three DWORD color masks that specify the red, green, and blue components, respectively, of each pixel. This is valid when used with 16- and 32-bpp bitmaps.
}

public struct CIEXYZ {
    public let x: UInt32 = 0
    public let y: UInt32 = 0
    public let z: UInt32 = 0
}

public struct CIEXYZTRIPLE {
    public let red = CIEXYZ()
    public let green = CIEXYZ()
    public let blue = CIEXYZ()
}

public struct RGBQuad {
    public let blue: UInt8 = 0
    public let green: UInt8 = 0
    public let red: UInt8 = 0
    public let reserved: UInt8 = 0
}

public class BitmapV4 {
    public let fileHeader: BitmapFileHeader
    public let infoHeader: BitmapV4Header
    public let bitmapData: ByteBuffer

    public init(fileHeader: BitmapFileHeader, infoHeader: BitmapV4Header, bitmapData: ByteBuffer) {
        self.fileHeader = fileHeader
        self.infoHeader = infoHeader
        self.bitmapData = bitmapData
    }
}
