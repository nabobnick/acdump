import Asheron
import Lilliput

public final class Convert {
    public func heightmap(cellFile: CellFile, destination: String) {
        let region = try! portalFile.fetchWorldRegion(handle: WorldRegionHandle(index: 0))
        let width = Int(UInt8.max)
        let height = Int(UInt8.max)
        let size = Int(region.heightsPerCell)
        let pixelWidth = width * size
        let pixelHeight = height * size
        let bitmapSize = pixelWidth * pixelHeight * PixelARGB8888.byteCount
        let bitmapData = OrderedByteBuffer<LittleEndian>(count: bitmapSize)
        var landBlocks = [[LandBlock]]()
        landBlocks.reserveCapacity(Int(width))
        
        for y in 0..<height {
            var xArray = [LandBlock]()
            xArray.reserveCapacity(height)
            
            for x in 0..<width {
                let position = CellPosition(x: x, y: y)
                let handle = LandBlockHandle(position: position)
                let landBlock = try! cellFile.fetchLandBlock(handle: handle)
                
                xArray.append(landBlock)
            }
            
            landBlocks.append(xArray)
        }
        
        for blockY in 0..<height {
            for y in 0..<size {
                for blockX in 0..<width {
                    let landBlock = landBlocks[(height - 1) - blockY][blockX]
                    
                    for x in 0..<size {
                        let pixel = PixelARGB8888(r: UInt8(blockX), g: UInt8((height - 1) - blockY), b: landBlock.getHeightIndex(x: x, y: (size - 1) - y), a: 255)
                        
                        bitmapData.putUInt32(pixel.argb8888.bits)
                    }
                }
            }
        }
        
        var infoHeader = BitmapV4Header()
        infoHeader.width = Int32(pixelWidth)
        infoHeader.height = -Int32(pixelHeight)
        infoHeader.sizeImage = UInt32(bitmapData.count)

        var fileHeader = BitmapFileHeader()
        let colorSize: UInt32 = 0
        fileHeader.size = BitmapFileHeader.byteSize + infoHeader.size + colorSize + UInt32(bitmapData.count)
        fileHeader.offBits = BitmapFileHeader.byteSize + infoHeader.size + colorSize

        let bitmap = BitmapV4(fileHeader: fileHeader, infoHeader: infoHeader, bitmapData: bitmapData.buffer)
        let bitmapWriter = BitmapWriter()
        let data = bitmapWriter.write(bitmap: bitmap)
        let name = destination + "/heightmap.bmp"

        do {
            let file = try BinaryFile.open(forWritingAtPath: name, create: true)
            let writeCount = try file.write(from: data)
            precondition(writeCount == data.count)
        }
        catch {
            print("Unable to write: \(name)")
            fatalError("\(error)")
        }    
    }

    public func map(portalFile: PortalFile, cellFile: CellFile, destination: String, namePrefix: String = "", spacing: Int = 0) {
        let region = try! portalFile.fetchWorldRegion(handle: WorldRegionHandle(index: 0))
        let numCellsX = Int(UInt8.max)
        let numCellsY = Int(UInt8.max)
        let pixelSize = Int(region.heightsPerCell)
        let pixelWidth = numCellsX * pixelSize
        let pixelHeight = numCellsY * pixelSize
        let spacingWidth = numCellsX * spacing
        let spacingHeight = numCellsY * spacing
        let totalWidth = pixelWidth + spacingWidth
        let totalHeight = pixelHeight + spacingHeight
        let bitmapSize = totalWidth * totalHeight * PixelARGB8888.byteCount
        let bitmapData = OrderedByteBuffer<LittleEndian>(count: bitmapSize)
        var landBlocks = [[LandBlock]]()
        landBlocks.reserveCapacity(Int(numCellsX))
        
        for y in 0..<numCellsY {
            var xArray = [LandBlock]()
            xArray.reserveCapacity(numCellsY)
            
            for x in 0..<numCellsX {
                let position = CellPosition(x: x, y: y)
                let handle = LandBlockHandle(position: position)
                let landBlock = try! cellFile.fetchLandBlock(handle: handle)
                
                xArray.append(landBlock)
            }
            
            landBlocks.append(xArray)
        }
        
        for blockY in 0..<numCellsY {
            for y in 0..<pixelSize {
                for blockX in 0..<numCellsX {
                    let landBlock = landBlocks[(numCellsY - 1) - blockY][blockX]
                    
                    for x in 0..<pixelSize {
                        let biomeIndex = Int(landBlock.getTopography(x: x, y: (pixelSize - 1) - y).biomeIndex)
                        let biome = region.biomes[biomeIndex]

                        bitmapData.putUInt32(biome.color.bits)
                    }

                    for _ in 0..<spacing {
                        bitmapData.putUInt32(0xFF000000)
                    }
                }
            }

            for _ in 0..<spacing {
                for _ in 0..<numCellsX {
                    for _ in 0..<pixelSize {
                        bitmapData.putUInt32(0xFF000000)
                    }

                    for _ in 0..<spacing {
                        bitmapData.putUInt32(0xFF000000)
                    }
                }
            }
        }
        
        var infoHeader = BitmapV4Header()
        infoHeader.width = Int32(totalWidth)
        infoHeader.height = -Int32(totalHeight)
        infoHeader.sizeImage = UInt32(bitmapData.count)

        var fileHeader = BitmapFileHeader()
        let colorSize: UInt32 = 0
        fileHeader.size = BitmapFileHeader.byteSize + infoHeader.size + colorSize + UInt32(bitmapData.count)
        fileHeader.offBits = BitmapFileHeader.byteSize + infoHeader.size + colorSize

        let bitmap = BitmapV4(fileHeader: fileHeader, infoHeader: infoHeader, bitmapData: bitmapData.buffer)
        let bitmapWriter = BitmapWriter()
        let data = bitmapWriter.write(bitmap: bitmap)
        let name = destination + "/\(namePrefix)_map.bmp"

        do {
            let file = try BinaryFile.open(forWritingAtPath: name, create: true)
            let writeCount = try file.write(from: data)
            precondition(writeCount == data.count)
        }
        catch {
            print("Unable to write: \(name)")
            fatalError("\(error)")
        }    
    }    
}
